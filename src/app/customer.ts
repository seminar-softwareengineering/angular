/**
 * Interface definiert die Attribute eines Kunden
 */
export interface Customer {
    name: string;
    city: string;
}