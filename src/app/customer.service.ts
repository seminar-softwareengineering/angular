import { Injectable } from '@angular/core';
import { Customer } from './customer';

/**
 * Service stellt Kunden bereit
 */
@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private customerList: Customer[];

  constructor() {
    // Daten werden hier statisch in einer Liste bereitgestellt. Üblicher Weise würde man sie aus einem REST-Service auslesen.
    this.customerList = [
      {
        "name": "Meier & Müller GmbH & Co KG",
        "city": "Hamburg"
      },
      {
        "name": "Elektro GmbH",
        "city": "Frankfurt"
      },
      {
        "name": "Telekomunikations AG",
        "city": "Bonn"
      }
    ];
  }

  /**
   * Liefert die Liste der Kunden
   */
  readCustomer(): Customer[] {
    console.log(this.customerList);
    return this.customerList;
  }

}