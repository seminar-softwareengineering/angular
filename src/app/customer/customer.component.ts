import { Component, Input, OnInit } from '@angular/core';
import { Customer } from '../customer';

/**
 * Komponente repräsentiert einen Kunden
 */
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {

  /**
   * Kundendaten
   */
  @Input()
  customer!: Customer;

  constructor() { }

  ngOnInit(): void {
  }

}
