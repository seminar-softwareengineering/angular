import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../customer.service';
import { Customer } from '../customer';

/**
 * Komponente repräsentiert eine Liste von Kunden
 */
@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  // Zur Vereinfachung werden hier die Daten als Konstanten angelegt
  customer1: Customer = {"name": "SoftDev GmbH", "city": "München"};
  customer2: Customer = {"name": "BIM AG", "city": "Berlin"};

  // Alternativ wird nachfolgend dargestellt, wie eine Liste von Kunden über einen Angular-Service befüllt werden kann
  customerList!: Customer[];

  constructor(
    // Injection des Kunden-Services. Angular instanziiert im Hintergrund den CustomerService und übergibt ihn in den Konstruktor
    private customerService: CustomerService
  ) { }

  ngOnInit(): void {
    // Auslesen der Kunden aus dem Angular-Service
    this.customerList = this.customerService.readCustomer();
  }

}
